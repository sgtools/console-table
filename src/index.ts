import { stdout } from 'process';
import { AppConsole, console } from '@sgtools/console';

export interface AppConsoleTableHeader
{
    col     : string;
    name    : string;
    flex    : number;
    truncate: boolean;
}

export const ConsoleTableAuto: AppConsoleTableHeader = {
    col     : 'auto',
    name    : 'N.',
    flex    : 0,
    truncate: false
};

export interface AppConsoleTableCell
{
    key     : string;
    value   : any;
}

export class AppConsoleTableRow
{
    cells   : AppConsoleTableCell[];

    constructor(cells: AppConsoleTableCell[])
    {
        this.cells = cells;
    }

    public get(key: string) : string
    {
        let cell = this.cells.find(c => c.key === key);
        if (!cell) return '';
        //if (Date.parse(cell.value.toString())) return (new Date(cell.value.toString())).toDateString();
        return cell.value.toString();
        //return cell ? cell.value.toString() : '';
    }
};

export class AppConsoleTable
{
    tableWidth  : number;
    rows        : AppConsoleTableRow[];
    headers     : AppConsoleTableHeader[];

    constructor(headers: AppConsoleTableHeader[], rows: AppConsoleTableRow[], width: number = 50)
    {
        this.headers = headers;
        this.rows = rows;
        this.tableWidth = (width === -1 ? stdout.columns : width);

        console.addPrivateColor('header', '38;5;231', 39);
        console.addPrivateColor('line', '48;5;233', 49);
    }

    private printHeader(widths: { key: string, value: number }[])
    {
        let headerLine = '';
        this.headers.forEach(header => {
            if (headerLine != '') headerLine += ' ';
            let width = widths.find(w => w.key === header.col).value;
            headerLine += "[b][u][header]" + header.name.padEnd(width) + "[/header][/u][/b]";
        });
        console.print(headerLine);
    }

    public show(showHeaders: boolean = true)
    {
        let widths: { key: string, value: number }[] = [];
        let flexes: { key: string, value: number }[] = [];
        let totalFlex = 0;
        let maxFlex = 0;
        let colMaxFlex = '';
    
        for (let i = 0; i < this.headers.length; i++) {
            let header = this.headers[i];
            widths.push({ key: header.col, value: header.name.length });
            if (header.col === 'auto' && this.rows.length > 99) {
                let index = widths.findIndex(w => w.key === header.col);
                widths[index].value = this.rows.length.toString().length;
            }
            flexes.push({ key: header.col, value: header.flex });
            totalFlex += header.flex;
            if (header.flex > maxFlex) {
                maxFlex = header.flex;
                colMaxFlex = header.col;
            }
        }

        for (let i = 0; i < this.rows.length; i++) {
            let row = this.rows[i];
            row.cells.forEach(cell => {
                let index = widths.findIndex(w => w.key === cell.key);
                let width = widths[index].value;
                if (cell.value.toString().length > width) {
                    widths[index].value = cell.value.toString().length;
                }
            });
        }

        let sum = this.headers.length - 1;
        widths.forEach(w => {
            sum += w.value;
        });
    
        if (sum <= this.tableWidth) {
    
            let span = this.tableWidth - sum;
            let cycles = Math.floor(span / totalFlex);
            for (let i = 0; i < widths.length; i++) {
                let col = widths[i].key;
                widths[i].value += cycles * flexes.find(f => f.key === col).value;
            }

            span -= cycles * totalFlex;
            if (span > 0) {
                let index = widths.findIndex(w => w.key === colMaxFlex);
                widths[index].value += span;
            }
    
            if (showHeaders) this.printHeader(widths);
    
            let lineCount = 0;
            this.rows.forEach(row => {
                let rowLine = '';
                this.headers.forEach(header => {
                    if (rowLine !== '') rowLine += ' ';
                    if (header.col === 'auto') {
                        lineCount++;
                        rowLine += lineCount.toString().padEnd(widths.find(w => w.key === header.col).value);
                    } else {
                        rowLine += row.get(header.col).padEnd(widths.find(w => w.key === header.col).value);
                    }
                });
                if (lineCount % 2 === 1) {
                    rowLine = "[line]" + rowLine + "[/line]";
                }
                console.print(rowLine);
            });

            if (lineCount === 0) console.print('[i]No data available[/i]');
    
        } else {

            let span = sum - this.tableWidth;
            let truncateValues: { key: string, value: number }[] = [];
            while (span > 0) {
                this.headers.forEach(header => {
                    if (span > 0 && header.truncate) {
                        let index = truncateValues.findIndex(t => t.key === header.col);
                        if (index === -1) {
                            truncateValues.push({ key: header.col, value: 1 });
                        } else {
                            truncateValues[index].value++;
                        }
                        span--;
                    }
                });
            }
            truncateValues.forEach(t => {
                let index = widths.findIndex(w => w.key === t.key);
                widths[index].value -= t.value;
            });
    
            if (showHeaders) this.printHeader(widths);
    
            let lineCount = 0;
            this.rows.forEach(row => {
                let rowLine = '';
                this.headers.forEach(header => {
                    if (rowLine !== '') rowLine += ' ';
                    if (header.col === 'auto') {
                        lineCount++;
                        rowLine += lineCount.toString().padEnd(widths.find(w => w.key === header.col).value);
                    } else {
                        let value = row.get(header.col);
                        let nValue = '';
                        if (value.length <= widths.find(w => w.key === header.col).value) {
                            nValue = value.padEnd(widths.find(w => w.key === header.col).value);
                        } else {
                            nValue = value.substr(0, widths.find(w => w.key === header.col).value - 3) + '...';
                        }
                        rowLine += nValue;
                    }
                });
                if (lineCount % 2 === 1) {
                    rowLine = "[line]" + rowLine + "[/line]";
                }
                console.print(rowLine);
            });
            
            if (lineCount === 0) console.print('[i]No data available[/i]');
    
        }
    }
};



function showArray(title: string, data: any[], showHeaders: boolean = true)
{
    let headers = [ ConsoleTableAuto, {
        col: 'A',
        name: title,
        flex: 1,
        truncate: true
    }];
    
    let rows = data.map(item => {
        return new AppConsoleTableRow([{
            key: 'A',
            value: item
        }]);
    });
   
    let table = new AppConsoleTable(headers, rows, -1);
    table.show(showHeaders);
}

function showObject(headersInfos: any, data: any[], showHeaders: boolean = true, options?: any)
{
    let lstHeaders = [];

    let headersTable = Object.keys(headersInfos).map(key => {
        lstHeaders.push(key);
        return {
            col: key,
            name: headersInfos[key],
            flex: 1,
            truncate: true
        };
    });

    if (options) {
        Object.keys(options).forEach(key => {
            let index = headersTable.findIndex(h => h.col === key);
            if (index !== -1) {
                let header = headersTable[index];
                Object.keys(options[key]).forEach(param => {
                    header[param] = options[key][param];
                });
            }
        });
    }

    let headers = [ ConsoleTableAuto, ...headersTable];
    
    let rows = data.map(item => {
        let lstItems = Object.keys(item).filter(key => lstHeaders.includes(key)).map(key => {
            return {
                key: key,
                value: item[key]
            };
        });
        return new AppConsoleTableRow([ ...lstItems ]);
    });
   
    let table = new AppConsoleTable(headers, rows, -1);
    table.show(showHeaders);
}



declare module '@sgtools/console'
{
    export interface AppConsole
    {
        table: {
            showArray(title: string, data: any[], showHeaders?: boolean): void;
            showObject(headersInfos: any, data: any[], showHeaders?: boolean, options?: any): void;
        }
    }
}
AppConsole.prototype.table = {
    showArray : (title: string, data: any[], showHeaders: boolean = true) =>
    {
        showArray(title, data, showHeaders)
    },
    showObject : (headersInfos: any, data: any[], showHeaders: boolean = true, options?: any) =>
    {
        showObject(headersInfos, data, showHeaders, options)
    }
}
