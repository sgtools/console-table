"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppConsoleTable = exports.AppConsoleTableRow = exports.ConsoleTableAuto = void 0;
const process_1 = require("process");
const console_1 = require("@sgtools/console");
exports.ConsoleTableAuto = {
    col: 'auto',
    name: 'N.',
    flex: 0,
    truncate: false
};
class AppConsoleTableRow {
    constructor(cells) {
        this.cells = cells;
    }
    get(key) {
        let cell = this.cells.find(c => c.key === key);
        if (!cell)
            return '';
        //if (Date.parse(cell.value.toString())) return (new Date(cell.value.toString())).toDateString();
        return cell.value.toString();
        //return cell ? cell.value.toString() : '';
    }
}
exports.AppConsoleTableRow = AppConsoleTableRow;
;
class AppConsoleTable {
    constructor(headers, rows, width = 50) {
        this.headers = headers;
        this.rows = rows;
        this.tableWidth = (width === -1 ? process_1.stdout.columns : width);
        console_1.console.addPrivateColor('header', '38;5;231', 39);
        console_1.console.addPrivateColor('line', '48;5;233', 49);
    }
    printHeader(widths) {
        let headerLine = '';
        this.headers.forEach(header => {
            if (headerLine != '')
                headerLine += ' ';
            let width = widths.find(w => w.key === header.col).value;
            headerLine += "[b][u][header]" + header.name.padEnd(width) + "[/header][/u][/b]";
        });
        console_1.console.print(headerLine);
    }
    show(showHeaders = true) {
        let widths = [];
        let flexes = [];
        let totalFlex = 0;
        let maxFlex = 0;
        let colMaxFlex = '';
        for (let i = 0; i < this.headers.length; i++) {
            let header = this.headers[i];
            widths.push({ key: header.col, value: header.name.length });
            if (header.col === 'auto' && this.rows.length > 99) {
                let index = widths.findIndex(w => w.key === header.col);
                widths[index].value = this.rows.length.toString().length;
            }
            flexes.push({ key: header.col, value: header.flex });
            totalFlex += header.flex;
            if (header.flex > maxFlex) {
                maxFlex = header.flex;
                colMaxFlex = header.col;
            }
        }
        for (let i = 0; i < this.rows.length; i++) {
            let row = this.rows[i];
            row.cells.forEach(cell => {
                let index = widths.findIndex(w => w.key === cell.key);
                let width = widths[index].value;
                if (cell.value.toString().length > width) {
                    widths[index].value = cell.value.toString().length;
                }
            });
        }
        let sum = this.headers.length - 1;
        widths.forEach(w => {
            sum += w.value;
        });
        if (sum <= this.tableWidth) {
            let span = this.tableWidth - sum;
            let cycles = Math.floor(span / totalFlex);
            for (let i = 0; i < widths.length; i++) {
                let col = widths[i].key;
                widths[i].value += cycles * flexes.find(f => f.key === col).value;
            }
            span -= cycles * totalFlex;
            if (span > 0) {
                let index = widths.findIndex(w => w.key === colMaxFlex);
                widths[index].value += span;
            }
            if (showHeaders)
                this.printHeader(widths);
            let lineCount = 0;
            this.rows.forEach(row => {
                let rowLine = '';
                this.headers.forEach(header => {
                    if (rowLine !== '')
                        rowLine += ' ';
                    if (header.col === 'auto') {
                        lineCount++;
                        rowLine += lineCount.toString().padEnd(widths.find(w => w.key === header.col).value);
                    }
                    else {
                        rowLine += row.get(header.col).padEnd(widths.find(w => w.key === header.col).value);
                    }
                });
                if (lineCount % 2 === 1) {
                    rowLine = "[line]" + rowLine + "[/line]";
                }
                console_1.console.print(rowLine);
            });
            if (lineCount === 0)
                console_1.console.print('[i]No data available[/i]');
        }
        else {
            let span = sum - this.tableWidth;
            let truncateValues = [];
            while (span > 0) {
                this.headers.forEach(header => {
                    if (span > 0 && header.truncate) {
                        let index = truncateValues.findIndex(t => t.key === header.col);
                        if (index === -1) {
                            truncateValues.push({ key: header.col, value: 1 });
                        }
                        else {
                            truncateValues[index].value++;
                        }
                        span--;
                    }
                });
            }
            truncateValues.forEach(t => {
                let index = widths.findIndex(w => w.key === t.key);
                widths[index].value -= t.value;
            });
            if (showHeaders)
                this.printHeader(widths);
            let lineCount = 0;
            this.rows.forEach(row => {
                let rowLine = '';
                this.headers.forEach(header => {
                    if (rowLine !== '')
                        rowLine += ' ';
                    if (header.col === 'auto') {
                        lineCount++;
                        rowLine += lineCount.toString().padEnd(widths.find(w => w.key === header.col).value);
                    }
                    else {
                        let value = row.get(header.col);
                        let nValue = '';
                        if (value.length <= widths.find(w => w.key === header.col).value) {
                            nValue = value.padEnd(widths.find(w => w.key === header.col).value);
                        }
                        else {
                            nValue = value.substr(0, widths.find(w => w.key === header.col).value - 3) + '...';
                        }
                        rowLine += nValue;
                    }
                });
                if (lineCount % 2 === 1) {
                    rowLine = "[line]" + rowLine + "[/line]";
                }
                console_1.console.print(rowLine);
            });
            if (lineCount === 0)
                console_1.console.print('[i]No data available[/i]');
        }
    }
}
exports.AppConsoleTable = AppConsoleTable;
;
function showArray(title, data, showHeaders = true) {
    let headers = [exports.ConsoleTableAuto, {
            col: 'A',
            name: title,
            flex: 1,
            truncate: true
        }];
    let rows = data.map(item => {
        return new AppConsoleTableRow([{
                key: 'A',
                value: item
            }]);
    });
    let table = new AppConsoleTable(headers, rows, -1);
    table.show(showHeaders);
}
function showObject(headersInfos, data, showHeaders = true, options) {
    let lstHeaders = [];
    let headersTable = Object.keys(headersInfos).map(key => {
        lstHeaders.push(key);
        return {
            col: key,
            name: headersInfos[key],
            flex: 1,
            truncate: true
        };
    });
    if (options) {
        Object.keys(options).forEach(key => {
            let index = headersTable.findIndex(h => h.col === key);
            if (index !== -1) {
                let header = headersTable[index];
                Object.keys(options[key]).forEach(param => {
                    header[param] = options[key][param];
                });
            }
        });
    }
    let headers = [exports.ConsoleTableAuto, ...headersTable];
    let rows = data.map(item => {
        let lstItems = Object.keys(item).filter(key => lstHeaders.includes(key)).map(key => {
            return {
                key: key,
                value: item[key]
            };
        });
        return new AppConsoleTableRow([...lstItems]);
    });
    let table = new AppConsoleTable(headers, rows, -1);
    table.show(showHeaders);
}
console_1.AppConsole.prototype.table = {
    showArray: (title, data, showHeaders = true) => {
        showArray(title, data, showHeaders);
    },
    showObject: (headersInfos, data, showHeaders = true, options) => {
        showObject(headersInfos, data, showHeaders, options);
    }
};
