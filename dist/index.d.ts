export interface AppConsoleTableHeader {
    col: string;
    name: string;
    flex: number;
    truncate: boolean;
}
export declare const ConsoleTableAuto: AppConsoleTableHeader;
export interface AppConsoleTableCell {
    key: string;
    value: any;
}
export declare class AppConsoleTableRow {
    cells: AppConsoleTableCell[];
    constructor(cells: AppConsoleTableCell[]);
    get(key: string): string;
}
export declare class AppConsoleTable {
    tableWidth: number;
    rows: AppConsoleTableRow[];
    headers: AppConsoleTableHeader[];
    constructor(headers: AppConsoleTableHeader[], rows: AppConsoleTableRow[], width?: number);
    private printHeader;
    show(showHeaders?: boolean): void;
}
declare module '@sgtools/console' {
    interface AppConsole {
        table: {
            showArray(title: string, data: any[], showHeaders?: boolean): void;
            showObject(headersInfos: any, data: any[], showHeaders?: boolean, options?: any): void;
        };
    }
}
