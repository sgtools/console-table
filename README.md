# Welcome to @sgtools/console-table
[![Version](https://npm.bmel.fr/-/badge/sgtools/console-table.svg)](https://npm.bmel.fr/-/web/detail/@sgtools/console-table)
[![Documentation](https://img.shields.io/badge/documentation-yes-brightgreen.svg)](https://gitlab.com/sgtools/console-table/-/blob/master/docs/README.md)
[![License: ISC](https://img.shields.io/badge/License-ISC-yellow.svg)](https://spdx.org/licenses/ISC)

> Console for nodejs applications
> Plugin : Tables


## Install

```sh
npm install @sgtools/console
npm install @sgtools/console-table
```

## Usage

```sh
import { console } from '@sgtools/console';
import '@sgtools/console-table';
```

Global import is required only once in projet.

Code documentation can be found [here](https://gitlab.com/sgtools/console-table/-/blob/master/docs/README.md).


## Author

**Sébastien GUERRI** <sebastien.guerri@apps.bmel.fr>


## Issues

Contributions, issues and feature requests are welcome!

Feel free to check [issues page](https://gitlab.com/sgtools/console-table/issues). You can also contact the author.


## License

This project is [ISC](https://spdx.org/licenses/ISC) licensed.
