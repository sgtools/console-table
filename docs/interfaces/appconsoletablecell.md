[@sgtools/console-table](../README.md) / [Exports](../modules.md) / AppConsoleTableCell

# Interface: AppConsoleTableCell

## Hierarchy

* **AppConsoleTableCell**

## Table of contents

### Properties

- [key](appconsoletablecell.md#key)
- [value](appconsoletablecell.md#value)

## Properties

### key

• **key**: *string*

Defined in: index.ts:21

___

### value

• **value**: *any*

Defined in: index.ts:22
