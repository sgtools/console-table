[@sgtools/console-table](../README.md) / [Exports](../modules.md) / AppConsoleTableHeader

# Interface: AppConsoleTableHeader

## Hierarchy

* **AppConsoleTableHeader**

## Table of contents

### Properties

- [col](appconsoletableheader.md#col)
- [flex](appconsoletableheader.md#flex)
- [name](appconsoletableheader.md#name)
- [truncate](appconsoletableheader.md#truncate)

## Properties

### col

• **col**: *string*

Defined in: index.ts:6

___

### flex

• **flex**: *number*

Defined in: index.ts:8

___

### name

• **name**: *string*

Defined in: index.ts:7

___

### truncate

• **truncate**: *boolean*

Defined in: index.ts:9
