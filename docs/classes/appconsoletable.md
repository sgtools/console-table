[@sgtools/console-table](../README.md) / [Exports](../modules.md) / AppConsoleTable

# Class: AppConsoleTable

## Hierarchy

* **AppConsoleTable**

## Table of contents

### Constructors

- [constructor](appconsoletable.md#constructor)

### Properties

- [headers](appconsoletable.md#headers)
- [rows](appconsoletable.md#rows)
- [tableWidth](appconsoletable.md#tablewidth)

### Methods

- [printHeader](appconsoletable.md#printheader)
- [show](appconsoletable.md#show)

## Constructors

### constructor

\+ **new AppConsoleTable**(`headers`: [*AppConsoleTableHeader*](../interfaces/appconsoletableheader.md)[], `rows`: [*AppConsoleTableRow*](appconsoletablerow.md)[], `width?`: *number*): [*AppConsoleTable*](appconsoletable.md)

#### Parameters:

Name | Type | Default value |
------ | ------ | ------ |
`headers` | [*AppConsoleTableHeader*](../interfaces/appconsoletableheader.md)[] | - |
`rows` | [*AppConsoleTableRow*](appconsoletablerow.md)[] | - |
`width` | *number* | 50 |

**Returns:** [*AppConsoleTable*](appconsoletable.md)

Defined in: index.ts:48

## Properties

### headers

• **headers**: [*AppConsoleTableHeader*](../interfaces/appconsoletableheader.md)[]

Defined in: index.ts:48

___

### rows

• **rows**: [*AppConsoleTableRow*](appconsoletablerow.md)[]

Defined in: index.ts:47

___

### tableWidth

• **tableWidth**: *number*

Defined in: index.ts:46

## Methods

### printHeader

▸ `Private`**printHeader**(`widths`: { `key`: *string* ; `value`: *number*  }[]): *void*

#### Parameters:

Name | Type |
------ | ------ |
`widths` | { `key`: *string* ; `value`: *number*  }[] |

**Returns:** *void*

Defined in: index.ts:60

___

### show

▸ **show**(`showHeaders?`: *boolean*): *void*

#### Parameters:

Name | Type | Default value |
------ | ------ | ------ |
`showHeaders` | *boolean* | true |

**Returns:** *void*

Defined in: index.ts:71
