[@sgtools/console-table](../README.md) / [Exports](../modules.md) / AppConsoleTableRow

# Class: AppConsoleTableRow

## Hierarchy

* **AppConsoleTableRow**

## Table of contents

### Constructors

- [constructor](appconsoletablerow.md#constructor)

### Properties

- [cells](appconsoletablerow.md#cells)

### Methods

- [get](appconsoletablerow.md#get)

## Constructors

### constructor

\+ **new AppConsoleTableRow**(`cells`: [*AppConsoleTableCell*](../interfaces/appconsoletablecell.md)[]): [*AppConsoleTableRow*](appconsoletablerow.md)

#### Parameters:

Name | Type |
------ | ------ |
`cells` | [*AppConsoleTableCell*](../interfaces/appconsoletablecell.md)[] |

**Returns:** [*AppConsoleTableRow*](appconsoletablerow.md)

Defined in: index.ts:27

## Properties

### cells

• **cells**: [*AppConsoleTableCell*](../interfaces/appconsoletablecell.md)[]

Defined in: index.ts:27

## Methods

### get

▸ **get**(`key`: *string*): *string*

#### Parameters:

Name | Type |
------ | ------ |
`key` | *string* |

**Returns:** *string*

Defined in: index.ts:34
