[@sgtools/console-table](README.md) / Exports

# @sgtools/console-table

## Table of contents

### Classes

- [AppConsoleTable](classes/appconsoletable.md)
- [AppConsoleTableRow](classes/appconsoletablerow.md)

### Interfaces

- [AppConsoleTableCell](interfaces/appconsoletablecell.md)
- [AppConsoleTableHeader](interfaces/appconsoletableheader.md)

### Variables

- [ConsoleTableAuto](modules.md#consoletableauto)

## Variables

### ConsoleTableAuto

• `Const` **ConsoleTableAuto**: [*AppConsoleTableHeader*](interfaces/appconsoletableheader.md)

Defined in: index.ts:12
